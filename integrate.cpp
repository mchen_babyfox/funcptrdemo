#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <iostream>

float gravity(float x)
{
	return 9.8;
}

float linear_function(float x)
{
	return 0.3*x;
}

float Integrate(float (*pfunc)(float), float x1, float x2, float dx)
{
	float integral = 0.0f;
	float x = x1;
	while(x < x2)
	{
		integral += pfunc(x)*dx;
		x += dx;
	}

	return integral;
}

int main(int argc, char** argv)
{
	std::cout<<"Velocity gained between (1, 3) " << Integrate(gravity, 1,3,0.001f)<<std::endl;
	std::cout<<"Integral 0.3x over (1, 3) " << Integrate(linear_function, 1,3,0.001f)<<std::endl;

	return 0;
}
