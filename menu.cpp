#include <stdlib.h>
#include <iostream>

struct MenuItem
{
	char name[16];			//name of the menu item
	void (*callback)(void* data);		//a callback function pointer
};

void CallbackStart(void* data)
{
	std::cout<<"Start is selected"<<std::endl;
}

void CallbackLoad(void* data)
{
	std::cout<<"Load is selected"<<std::endl;
}

void CallbackQuit(void* data)
{
	std::cout<<"Quit is selected"<<std::endl;
	*((bool*)data) = true;
}

void MenuListener(const MenuItem* menu, void* data)
{
	menu->callback(data);
}

void DisplayMenu(MenuItem* menu, int count)
{
	for(int i = 0; i < count; i++)
	{
		std::cout<<"["<<i+1<<"] "<<(menu+i)->name<<std::endl<<std::flush;
	}
}

int main(int argc, char** argv)
{
	MenuItem menu[3] = 
	{
		{"Start", CallbackStart},
		{"Load", CallbackLoad},
		{"Quit", CallbackQuit}
	};
	
	bool quit = false;

	while(!quit)
	{
		int selection;

		DisplayMenu(menu, 3);
		
		std::cin>>selection;
		
		if (selection > 0 && selection < 4)
		{
			int index = selection - 1;

			MenuListener(&menu[index], &quit);
		}
	}

	return 0;
}
