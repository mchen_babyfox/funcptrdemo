#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <iostream>

enum ECalcOperator
{
	ADD,
	SUB,
	MUL,
	DIV
};

float add(float lhs, float rhs)
{
	return lhs + rhs;
}

float sub(float lhs, float rhs)
{
	return lhs - rhs;
}

float mul(float lhs, float rhs)
{
	return lhs * rhs;
}

float div(float lhs, float rhs)
{
	return lhs / rhs;
}

float hypotenuse(float lhs, float rhs)
{
	return sqrtf(lhs*lhs + rhs*rhs);
}

float Calculate(ECalcOperator op, float lhs, float rhs)
{
	switch(op)
	{
		case ADD:
			return lhs + rhs;
		case SUB:
			return lhs - rhs;
		case MUL:
			return lhs * rhs;
		case DIV:
			return lhs / rhs;
		default:
			break;
	}
	return 0; //reasonable?
}

float Calculate(float (*pfunc)(float, float), float lhs, float rhs)
{
	return pfunc(lhs, rhs);
}

int main(int argc, char** argv)
{
	float a = 10;
	float b = 1001;
	//Call using enum	
	std::cout<<"a ADD b "<<Calculate(ADD, a, b)<<std::endl;
	std::cout<<"a SUB b "<<Calculate(SUB, a, b)<<std::endl;
	std::cout<<"a MUL b "<<Calculate(MUL, a, b)<<std::endl;
	std::cout<<"a DIV b "<<Calculate(DIV, a, b)<<std::endl;

	//Call using function pointers
	std::cout<<"a ADD b "<<Calculate(add, a, b)<<std::endl;
	std::cout<<"a SUB b "<<Calculate(sub, a, b)<<std::endl;
	std::cout<<"a MUL b "<<Calculate(mul, a, b)<<std::endl;
	std::cout<<"a DIV b "<<Calculate(div, a, b)<<std::endl;
	std::cout<<"Hypotenuse of (a b) "<<Calculate(hypotenuse, a, b)<<std::endl;

	return 0;
}
